(function ($) {
  // Use strict mode to avoid errors:
  // https://developer.mozilla.org/en/JavaScript/Strict_mode
  "use strict";

  Drupal.behaviors.botrfield_jwplayer = {
    attach: function (context, settings) {
      $('.jw-player-video').once(function(){
        var player_div = $(this);
        var id = player_div.attr('id');
        if (Drupal.settings.botrfield_jwplayer[id] != undefined) {
          jwplayer(id).setup(Drupal.settings.botrfield_jwplayer[id]);
        }
      })
    }
  }
})(jQuery);


